class Activate2020 < ActiveRecord::Migration
  def change
      Semester.all.each do |s|
	  if (s.year == 2020 and s.semester == 1)
	     s.update_attributes :active => true
	  else
	     s.update_attributes :active => false
	  end
	  s.save
      end	
  end
end
