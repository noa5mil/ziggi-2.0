class AddSemesters2019 < ActiveRecord::Migration
def up
       Semester.all.each do |s|
	     s.update_attributes :active => false
	  end

    Semester.create([
	{   :university_id => 1, :name => 'סתיו תשע"ט',
	    :year => 2019, :semester => 1,
	    :start => DateTime.new(2018, 10, 14),
	    :end => DateTime.new(2019, 1, 11),
	    :exams_start => DateTime.new(2019, 1, 13),
	    :exams_end => DateTime.new(2019, 2, 22),
	    :active => true
	},
	{   :university_id => 1, :name => 'אביב תשע"ט',
	    :year => 2019, :semester => 2,
	    :start => DateTime.new(2019, 2, 24),
	    :end => DateTime.new(2019, 6, 21),
	    :exams_start => DateTime.new(2019, 6, 23),
	    :exams_end => DateTime.new(2019, 8, 2),
	    :active => false
	}
    ])
  end
end
