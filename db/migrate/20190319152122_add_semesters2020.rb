class AddSemesters2020 < ActiveRecord::Migration
def up
       Semester.all.each do |s|
	     s.update_attributes :active => false
	  end

    Semester.create([
	{   :university_id => 1, :name => 'סתיו תש"פ',
	    :year => 2020, :semester => 1,
	    :start => DateTime.new(2019, 10, 27),
	    :end => DateTime.new(2020, 1, 24),
	    :exams_start => DateTime.new(2020, 1, 26),
	    :exams_end => DateTime.new(2020, 3, 6),
	    :active => true
	},
	{   :university_id => 1, :name => 'אביב תש"פ',
	    :year => 2020, :semester => 2,
	    :start => DateTime.new(2020, 3, 8),
	    :end => DateTime.new(2020, 6, 26),
	    :exams_start => DateTime.new(2020, 6, 28),
	    :exams_end => DateTime.new(2020, 8, 14),
	    :active => false
	}
    ])
  end
end
