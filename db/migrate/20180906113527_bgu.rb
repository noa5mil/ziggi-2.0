class Bgu < ActiveRecord::Migration
	def up
		    University.create([
			  { :code => "bgu", :name => "אוניברסיטת בן גוריון שבנגב",
			      :homepage => "http://www.bgu.ac.il",
			      :lat => 31.262425, :lng => 34.801919, :status => 1 }
			  ])
	end
end
